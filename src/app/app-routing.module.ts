import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarteDesVinsComponent } from './pages/carte-des-vins/carte-des-vins.component';
import { CartesComponent } from './pages/cartes/cartes.component';
import { ConceptComponent } from './pages/concept/concept.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { LaBaladeComponent } from './pages/la-balade/la-balade.component';
import { LegalNoticeComponent } from './pages/legal-notice/legal-notice.component';
import { LieuComponent } from './pages/lieu/lieu.component';
import { MenuComponent } from './pages/menu/menu.component';
import { ReservationComponent } from './pages/reservation/reservation.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'la-balade', component: LaBaladeComponent },
  { path: 'concept', component: ConceptComponent },
  { path: 'lieu', component: LieuComponent },
  { path: 'reservation', component: ReservationComponent },
  { path: 'cartes', component: CartesComponent },
  { path: 'carte-des-vins', component: CarteDesVinsComponent},
  { path: 'menu', component: MenuComponent},
  { path: 'contact', component: ContactComponent },
  { path: 'legal-notice', component: LegalNoticeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

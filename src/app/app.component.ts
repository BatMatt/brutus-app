import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationStart, Router, RouterEvent } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

declare let gtag: Function;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'brutus';

  heroImgUrl: string;

  mobileMenuToggle = false;

  currentLang = 'fr';

  showOverlay: boolean;


  constructor(
    private router: Router,
    private translate: TranslateService
  ) {
    this.showOverlay = true;
  }

  ngAfterViewInit() {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.showOverlay = true;
        }
        else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel
        ) {
          this.showOverlay = false;
        }
      });
  }


  ngOnInit() {
    this.translate.setDefaultLang('fr');
    this.translate.use('fr');
    this.translate.onLangChange.subscribe(lang => this.currentLang = lang.lang);
    this.router.events.subscribe((event: RouterEvent) => {

      // this.navigationInterceptor(event)


      if (event instanceof NavigationEnd) {

        gtag('config', 'G-YBPP3CJZVX',
          {
            page_path: event.urlAfterRedirects
          }
        );

        this.mobileMenuToggle = false;

        switch (event.url) {
          case '/la-balade':
            this.heroImgUrl = "../assets/img/_III9324.jpg";
            break;
          case '/concept':
            this.heroImgUrl = "../assets/img/_III0508.jpg";
            break;
          case '/lieu':
            this.heroImgUrl = "../assets/img/_III9964.jpg";
            break;
          case '/reservation':
            this.heroImgUrl = "../assets/img/_III9043.jpg";
            break;
          case '/cartes':
            this.heroImgUrl = "../assets/img/_III0157.jpg";
            break;
          case '/carte-des-vins':
            this.heroImgUrl = "../assets/img/_III0157.jpg";
            break;
          case '/menu':
            this.heroImgUrl = "../assets/img/_III0157.jpg";
            break;
          case '/contact':
            this.heroImgUrl = "../assets/img/_III9539.jpg";
            break;
          default:
            this.heroImgUrl = "../assets/img/_III9362.jpg";
        }
      }
    });
  }
  useLanguage(language: string): void {
    this.translate.use(language);
  }

}

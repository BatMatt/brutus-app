import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LaBaladeComponent } from './pages/la-balade/la-balade.component';
import { ConceptComponent } from './pages/concept/concept.component';
import { LieuComponent } from './pages/lieu/lieu.component';
import { ReservationComponent } from './pages/reservation/reservation.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CartesComponent } from './pages/cartes/cartes.component';
import { HomeComponent } from './pages/home/home.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LegalNoticeComponent } from './pages/legal-notice/legal-notice.component';
import { MenuComponent } from './pages/menu/menu.component';
import { CarteDesVinsComponent } from './pages/carte-des-vins/carte-des-vins.component';

@NgModule({
  declarations: [
    AppComponent,
    LaBaladeComponent,
    ConceptComponent,
    LieuComponent,
    ReservationComponent,
    ContactComponent,
    CartesComponent,
    HomeComponent,
    LegalNoticeComponent,
    MenuComponent,
    CarteDesVinsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
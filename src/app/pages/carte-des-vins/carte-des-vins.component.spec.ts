import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarteDesVinsComponent } from './carte-des-vins.component';

describe('CarteDesVinsComponent', () => {
  let component: CarteDesVinsComponent;
  let fixture: ComponentFixture<CarteDesVinsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarteDesVinsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarteDesVinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

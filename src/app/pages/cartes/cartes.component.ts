import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-cartes',
  templateUrl: './cartes.component.html',
  styleUrls: ['./cartes.component.scss']
})
export class CartesComponent implements OnInit {

  menuString = "MENU BRUTUS FR";

  constructor(private translateService: TranslateService) { }

  ngOnInit(): void {
    if (this.translateService.currentLang === 'en') {
      this.menuString = "MENU BRUTUS EN";
    }

    this.translateService.onLangChange.subscribe(lang => {
      if (lang.lang === 'en') {
        this.menuString = "MENU BRUTUS EN";
      } else if (lang.lang === 'fr') {
        this.menuString = "MENU BRUTUS FR";
      }
    })
  }

}

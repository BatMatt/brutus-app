import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LaBaladeComponent } from './la-balade.component';

describe('LaBaladeComponent', () => {
  let component: LaBaladeComponent;
  let fixture: ComponentFixture<LaBaladeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LaBaladeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LaBaladeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

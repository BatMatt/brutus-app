module.exports = {
  purge: ['./src/**/*.html', './src/**/*.ts'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'camel': '#A8692A',
        'beige': '#F3E5D4',
        'grey-green': '#687C80',
        'blue-duck': '#285763',
        'dark-grey': '#282828',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}